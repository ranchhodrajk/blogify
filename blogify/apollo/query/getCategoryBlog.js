import { gql } from "@apollo/client";
export const GETCATEGORYBLOG = gql`
  query GetTagPosts($tag: String!) {
    getTagPosts(tag: $tag) {
      id
      body
      username
      created_at
      title
      tags {
        name
      }
      likeCount
      commentCount
      likes {
        id
        username
        created_at
      }
      comments {
        id
        body
        username
        created_at
      }
    }
  }
`;
