import React,{useEffect} from "react";
import s from "../../styles/CategoryDetail.module.scss";
import { Row, Col, Tooltip, Spin, Empty, Button } from "antd";
import MyBlogCard from "../../components/MyBlogCard/MyBlogCard";
import { useQuery } from "@apollo/client";
import { GETCATEGORYBLOG } from "../../apollo/query/getCategoryBlog";
import AOS from 'aos';

const Category = ({ categoryName }) => {
  const { loading, error, data, refetch } = useQuery(GETCATEGORYBLOG, {
    variables: { tag: categoryName },
  });

  useEffect(() => {
    AOS.init();
  }, [])


  return (
    <div className={s.mainMyBlogContainer}>
      {loading ? (
        <div className={`${s.loadDiv} mySpinCustom`}>
          <Spin size="large" />
        </div>
      ) : (
        <Row justify="center" align="middle">
          <Col xs={20} style={{overflow:'hidden'}}>
            <div className={s.head}>
              <div className={s.headMain}>
                {categoryName?.charAt(0)?.toUpperCase() +
                  categoryName?.slice(1)}
              </div>
              <div className={s.headSub}>
                Specific blogs in that you are intrested
              </div>
            </div>
            {data?.getTagPosts?.length > 0 ? (
              data?.getTagPosts?.map((item, index) => (
                <div key={index} data-aos={index%2=== 0 ? 'fade-right':'fade-left'}>
                  <MyBlogCard
                    id={item?.id}
                    index={index}
                    title={item?.title}
                    body={item?.body}
                    created_at={item?.created_at}
                    likeCount={item?.likeCount}
                    commentCount={item?.commentCount}
                    userName={item?.username}
                    key={index}
                    likeArr={item?.likes}
                  />
                </div>
              ))
            ) : (
              <div className={s.emptyBox}>
                <Empty
                  image="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"
                  imageStyle={{
                    height: 60,
                  }}
                  description={
                    <span style={{ color: "grey" }}>Nothig to show</span>
                  }
                ></Empty>
              </div>
            )}
          </Col>
        </Row>
      )}
    </div>
  );
};

export default Category;

export async function getServerSideProps(context) {
  const { category } = context.query;

  return {
    props: {
      categoryName: category,
    },
  };
}
