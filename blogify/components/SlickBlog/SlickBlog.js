import React from "react";
import s from "../../styles/SlickBlog.module.scss";
import Slider from "react-slick";
import Image from "next/image";
import { Button } from "antd";
import Link from "next/link";
import { getCookie } from "../../utilities/cookies";
import { useRouter } from "next/router";
import { useSelector, useDispatch } from "react-redux";
import { showSignInModel } from "../../Reducers/showSignInModel";

const SlickBlog = () => {

  const router = useRouter();
  const dispatch = useDispatch();
  const { isShowModel } = useSelector((state) => state.signInModel);

  const settings = {
    infinite: true,
    slidesToShow: 1,
    arrows: false,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    cssEase: "linear",
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: false,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 1,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  const onClickSlidBtn = () => {
    const getToken = getCookie("token");

    const isLog = localStorage.getItem("isLoggin");
    const isLoggin = JSON.parse(isLog);

    if (getToken === null || isLoggin === null) {
      dispatch(showSignInModel(!isShowModel));
    }
    else{
      router.push('/writeBlog')
    }
  };

  return (
    <div className={s.mainSlickBlogSlider}>
      <Slider {...settings}>
        <div className={s.upperSlide1}>
          <div className={s.upper}>
            <div className={s.imageNext}>
              <Image src="/Assets/blog8.jpg" alt="" layout="fill" />
            </div>
            <div className={s.dark}></div>
            <div className={s.containt}>
              <div>
                <div className={s.head}>Informative blogs with you</div>
                <div className={s.des}>
                  Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                  Sunt, asperiores.
                </div>
                
                <div className={s.menuItem}>
                  <button onClick={onClickSlidBtn}>Write blog</button>
                </div>
                
              </div>
            </div>
          </div>
        </div>
        <div className={s.upperSlide1}>
          <div className={s.upper}>
            <div className={s.imageNext}>
              <Image src="/Assets/first7.jpg" alt="" layout="fill" />
            </div>
            <div className={s.dark}></div>
            <div className={s.containt}>
              <div>
                <div className={s.head}>Blogging is good for you </div>
                <div className={s.des}>
                  Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                  Sunt, asperiores.
                </div>
                
                    <div className={s.menuItem}>
                      <button onClick={onClickSlidBtn}>Write blog</button>
                    </div>
                  
              </div>
            </div>
          </div>
        </div>
        <div className={s.upperSlide1}>
          <div className={s.upper}>
            <div className={s.imageNext}>
              <Image src="/Assets/first4.jpg" alt="" layout="fill" />
            </div>
            <div className={s.dark}></div>
            <div className={s.containt}>
              <div>
                <div className={s.head}>Buy attention with blog</div>
                <div className={s.des}>
                  Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                  Sunt, asperiores.
                </div>
                
                    <div className={s.menuItem}>
                      <button onClick={onClickSlidBtn}>Write blog</button>
                    </div>
                 
              </div>
            </div>
          </div>
        </div>
      </Slider>
    </div>
  );
};

export default SlickBlog;
